---
title: "First post in 2018 😝!"
date: 2018-01-17T21:57:29+08:00
thumbnailImage: https://i.pinimg.com/originals/b1/fa/26/b1fa264aca3e5176da5648c06311ebd4.jpg
thumbnailImagePosition: right
categories:
- The adventure of life
tags:
- Life
- Problems
- Hugo
---
## The introduction of me

Hi there !

This is my first post in 2018.

Actually I have built more than 3 blogs so far, but none of them being maintained well. I do believe 2018 is a  different year for me, since I have lots of goals and challenges that I need to achieve and overcome, I definitely will share some of the thoughts here along the way to help me and you guys growing up and staying young.



> Observing like a newborn baby
>
> Thinking like an alien creature  
> Responding like a human being
>
> Me @ 2014

---



## The problem I encountered during blog construction (Hugo)

Here are some tips that you should notice if you planing to build a blog based on hugo and deploy it on gitlab:

1. Just follow the tips on the official website

  *There are maybe tons of instructions shared by other people on the internet, but some of them were outdated, you may encounter lots of unknown problems if you follow their instructions*

2. Read the instructions carefully, and be patient

  *Any stupid mistakes can be made if you are careless, if you have problem when you trying to add a theme and configure it, you better read the README file of that theme first, or you will meet annoying bugs.*


Actually I started to build this blog at Jan 1 2018, however, I only deploy it untill today 🙃, that's because I didn't follow the rules that written above. I have met 2 major problems: 

1. Cannot change the cover image

  *I once uploded my cover image to the image file where the original author put into, it worked locally when ran the server test, but I didn't go through the notice that the author left* **(I cannot just use it that way, because if I push it to the gitlab, the theme will automatically linked to the original repository created by author, not the theme that modified by me locally, which means the images that I uploaded will stay at my computer forever, it will not be push to gitlab. This actually took me 2 weeks gap to investigate.)**

2. Fatal when push to gitlab

  *The warning shows like this*

  	The authenticity of host 'gitlab.com (52.167.219.168)' can't be established.
  	ECDSA key fingerprint is SHA256:HbW3g8zUjNSksFbqTiUWPWg2Bq1x8xdGUrliXFzSnUw.
  	Are you sure you want to continue connecting (yes/no)?

  *Which means the url that you use to git add to remote is not correct, I used `git@gitlab.com:` format from the instruction on the internet, which caused that problem, It tooks me several hours to figured that out (1 day gap), because I reveiw the instruction of the official website, the url it suggest is `https://gitlab.com/yourname/your_repo` format. I fixed it and succeed.*


Hope you  guys enjoy my first post, and help me correct my grammar error :).





