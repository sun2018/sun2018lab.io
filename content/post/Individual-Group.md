---
title: "Individual Group"
date: 2018-03-21T11:32:22+08:00
categories:
- Quotes
tags:
- Jordan B Peterson
---

> A close reading of 20th century history, indicates, as nothing else can. The horrors that accompany loss of faith in the idea of the individual, it is only the individual after all who suffers, the group does not suffer, only those who compose it. Thus the reality of the individual must be regarded as primary if suffering to be regarded seriously. Without such regard, there can be no motivation to reduce suffering, and therefore no respite. Instead, the production of individual can and has and will be again rationalized. It justified for its supposed to be benefits for the future and the group.

 

— — Jordan B Peterson

 

 

仔细阅读20世纪的历史，也只有它可以表明。那个伴随着对个体的理念失去信念的种种惨剧，最终也只有个体才会遭受苦难, 群体是不会的，只有组成群体的个人。 因此苦难要被认真对待的话，个体的现实情况必须被视为最首要的。如果苦难不被认真对待，那么就没有减少苦难的动力，因此我们也就没有喘息的机会。相反的，那些被人为制造的苦难能被，已经被，并且会再度被合理化，它会被解释为，是为了那些所谓的集体和未来的利益。