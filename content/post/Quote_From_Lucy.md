---
title: "Lucy Quotes"
date: 2018-01-27T17:21:21+08:00
thumbnailImage: https://i.pinimg.com/564x/d2/f8/a5/d2f8a5146c6df03efabf0d818806f2b6.jpg
thumbnailImagePosition: right
categories:
- Quotes
tags:
- Lucy (Movie)
---

> **Learning is always a painful process.** Like when you're little and your bones are growing and you ache all over. Do you believe I can remember the sound are music that I can understand, like fluids. It's funny, I used to be so concerned with who I was and what I wanted to be, and now that I have access to the furthest reaches of my brain, I see things clearly and realize that what makes us "us" — ***it's primitive***. **They're all obstacles. Does that make any sense?**
> 
> Like this [pain](https://en.wikiquote.org/wiki/Pain) you're experiencing. **It's blocking you from understanding**. All you know now is pain. That's all you know, *pain*.
>
> — From Lucy (2014 film)