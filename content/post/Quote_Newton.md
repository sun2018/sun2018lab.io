---
title: "Quote Newton"
date: 2018-01-18T16:43:01+08:00
thumbnailImage: http://jazjaz.net/wp-content/uploads/2016/03/Asya-Lisina-Isaac-Newton-Illustration.jpg
thumbnailImagePosition: right
categories:
- Quotes
tags:
- Newton
---

>  "When you come into any fresh company, observe their humours. **Suit your own carriage accordingly**, and in such a way that you will make them more free and open in conversation. Let your discussions *consist more of* questions and doubts than absolute *assertions*  or arguments,  **it being the travelers to learn, not to teach**. Besides, it will persuade your acquaintances that you have the greater esteem of them, and so make them more ready to communicate what they know to you; **wheras nothing sooner occasions disrespect and quarrels than arrogance and boldness**. You will find little or no advantage in seeming wiser, or much more ignorant than your company. Seldom criticize anything too harshly, or, if you must, do it moderately, **lest** you be unexpectedly  forced to embarrassingly take back your words. **It is safer to praise meets less often with opposition**, or, at least, is not usually so **ill resented** by men that think otherwise; and you will gain men's favor by nothing sooner than seeming to approve what they like; but beware of doing that comparisonly."
>

 — Sir Isaac Newton to one of his pupils

